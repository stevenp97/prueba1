FROM openjdk:8-slim-buster

LABEL maintainer="steven.pedroza@salesianoslosboscos.com"

RUN apt-get update && apt-get install -y curl net-tools procps iputils-ping \
 && rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/apt/* \
 && apt-get autoremove -y && apt-get autoclean
